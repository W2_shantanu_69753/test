import React, { useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

function ProfileDetail({ profile, onClose }) {
  const [showMap, setShowMap] = useState(false);

  const handleShowMap = () => {
    setShowMap(true);
  };

  return (
    <div>
      <h2>Profile Details</h2>
      <p>Name: {profile.name}</p>
      <p>Description: {profile.description}</p>
      <p>Address: {profile.address}</p>
      <button onClick={onClose}>Close</button>
      <button onClick={handleShowMap}>Summary</button>

      {showMap && (
        <div>
          <h2>Location Summary</h2>
          <MapContainer center={[profile.coordinates.latitude, profile.coordinates.longitude]} zoom={15} style={{ height: '400px' }}>
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            />
            <Marker position={[profile.coordinates.latitude, profile.coordinates.longitude]}>
              <Popup>{profile.name}'s Location</Popup>
            </Marker>
          </MapContainer>
        </div>
      )}
    </div>
  );
}

export default ProfileDetail;
