import React, { useState, useEffect } from 'react';
import sampleProfiles from './profiles.json'; // Import the sample JSON data

function ProfileList() {
  const [profiles, setProfiles] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [selectedProfile, setSelectedProfile] = useState(null);

  useEffect(() => {
    
    setProfiles(sampleProfiles);
   
  }, []);

  const filteredProfiles = profiles.filter((profile) =>
    profile.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSummaryClick = (profile) => {
    setSelectedProfile(profile);
  };

  return (
    <div>
      <h1>Profile List</h1>
      <ul>
        {profiles.map((profiles) => (
          <li key={profiles.id}>
            <h2>{profiles.name}</h2>
            <p>{profiles.description}</p>
            <p>{profiles.address}</p>
            <button onClick={() => handleSummaryClick(profiles)}>Summary</button>
          </li>
        ))}
      </ul>
      <input
        type="text"
        placeholder="Search profiles by name"
        value={searchTerm}
        onChange={handleSearch}
      />
      <ul>
        {filteredProfiles.map((profile) => (
          <li key={profile.id}>
            <h2>{profile.name}</h2>
            <p>{profile.description}</p>
            <p>{profile.address}</p>
            <button onClick={() => handleSummaryClick(profile)}>Summary</button>
          </li>
        ))}
      </ul>
      {selectedProfile && (
        <div>
          <h2>Profile Details</h2>
          <p>Name: {selectedProfile.name}</p>
          <p>Description: {selectedProfile.description}</p>
          <p>Address: {selectedProfile.address}</p>
          <button onClick={() => setSelectedProfile(null)}>Close</button>
        </div>
      )}
    </div>
  );
}

export default ProfileList;




