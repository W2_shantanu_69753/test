import React, { useState, useEffect } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

function MapView({ profiles }) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    setError(null);

    setTimeout(() => {
      setLoading(false);
     
    }, 2000); 
  }, []);

  return (
    <div>
      <h1>Map View</h1>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p>Error: {error.message}</p>
      ) : (
        <MapContainer center={[0, 0]} zoom={2} style={{ height: '500px' }}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          />
          {profiles.map((profile) => (
            <Marker
              key={profile.id}
              position={[profile.coordinates.latitude, profile.coordinates.longitude]}
            >
              <Popup>{profile.name}'s Location</Popup>
            </Marker>
          ))}
        </MapContainer>
      )}
    </div>
  );
}

export default MapView;
