import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ProfileList from './ProfileList';
import ProfileDetail from './ProfileDetail';
import MapView from './MapView';
import AdminPanel from './AdminPanel';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/profile/:id" component={ProfileDetail} />
        <Route path="/map" component={MapView} />
        <Route path="/admin" component={AdminPanel} />
        <Route path="/" exact component={ProfileList} />
      </Routes>
    </Router>
  );
}

export default App;
