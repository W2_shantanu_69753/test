import React, { useState } from 'react';

function AdminPanel() {
  const [profiles, setProfiles] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    description: '',
    address: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleAddProfile = () => {
    if (!formData.name || !formData.address) {
      alert('Name and Address are required fields.');
      return;
    }

    
    const newProfile = {
      id: profiles.length + 1, 
      ...formData,
    };

  
    setProfiles([...profiles, newProfile]);

   
    setFormData({
      name: '',
      description: '',
      address: '',
    });
  };

  const handleEditProfile = (id) => {
  };

  const handleDeleteProfile = (id) => {
    
  };

  return (
    <div>
      <h1>Admin Panel</h1>
      <div>
        <h2>Add/Edit Profile</h2>
        <input
          type="text"
          name="name"
          placeholder="Name"
          value={formData.name}
          onChange={handleInputChange}
        />
        <input
          type="text"
          name="description"
          placeholder="Description"
          value={formData.description}
          onChange={handleInputChange}
        />
        <input
          type="text"
          name="address"
          placeholder="Address"
          value={formData.address}
          onChange={handleInputChange}
        />
        <button onClick={handleAddProfile}>Add Profile</button>
      </div>
      <div>
        <h2>Profile List</h2>
        <ul>
          {profiles.map((profile) => (
            <li key={profile.id}>
              <p>Name: {profile.name}</p>
              <p>Description: {profile.description}</p>
              <p>Address: {profile.address}</p>
              <button onClick={() => handleEditProfile(profile.id)}>Edit</button>
              <button onClick={() => handleDeleteProfile(profile.id)}>Delete</button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default AdminPanel;
